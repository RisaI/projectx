﻿using System;
using FarseerPhysics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using FarseerPhysics.Common;
using FarseerPhysics.Collision;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace projectx.Game.Entities
{
	public class BodyComponent : EntityComponent
	{
		public Body EntityBody {
			get;
			private set;
		}

		protected List<Vertices> Vertices;
		protected Vector2 centroid;

		public BodyComponent ()
		{
			
		}

		public BodyComponent(params Vector2[] vertices) {
			var verts = new Vertices (vertices);
			centroid = verts.GetCentroid ();
			Vertices = FarseerPhysics.Common.Decomposition.Triangulate.ConvexPartition (verts, FarseerPhysics.Common.Decomposition.TriangulationAlgorithm.Bayazit);
		}

		public override EntityComponent SetParent (Entity ent)
		{
			if (Parent != null) {
				Parent.EventDispatcher.UnregisterEvent ("preupdate", UpdateEntityPosition);
				Parent.EventDispatcher.UnregisterEvent ("setpos", OnSetPosition);
				Parent.EventDispatcher.UnregisterEvent ("setrot", OnSetRotation);
				Parent.EventDispatcher.UnregisterEvent ("remove", OnRemoval);
			}
			ent.EventDispatcher.RegisterEvent ("preupdate", UpdateEntityPosition);
			ent.EventDispatcher.RegisterEvent ("setpos", OnSetPosition);
			ent.EventDispatcher.RegisterEvent ("setrot", OnSetRotation);
			ent.EventDispatcher.RegisterEvent ("remove", OnRemoval);

			if (EntityBody != null)
				ent.World.Physics.RemoveBody (EntityBody);
			
			if (Vertices != null) {
				EntityBody = BodyFactory.CreateCompoundPolygon (ent.World.Physics, Vertices, 1, ent.Position / 64f, ent.Rotation, BodyType.Dynamic, ent);
				EntityBody.LocalCenter = centroid;
			}
			else
				EntityBody = BodyFactory.CreateRectangle (ent.World.Physics, 1, 1, 1, ent.Position / 64f, ent.Rotation, BodyType.Dynamic, ent);
			EntityBody.OnCollision += OnCollision;
			EntityBody.OnSeparation += OnSeparation;
			return base.SetParent (ent);
		}

		void OnSeparation (Fixture fixtureA, Fixture fixtureB)
		{
			Parent.EventDispatcher.Invoke ("separation", new Fixture[] { fixtureA, fixtureB });
		}

		void OnRemoval(object obj) {
			Parent.World.Physics.RemoveBody (EntityBody);
			EntityBody.Dispose ();
		}

		void UpdateEntityPosition(object obj) {
			if (EntityBody != null) {
				Parent.Position = EntityBody.Position * 64f;
				Parent.Rotation = EntityBody.Rotation;
			}
		}

		void OnSetPosition(object p) {
			if (EntityBody != null)
				EntityBody.Position = ((Vector2)p) * 64f;
		}

		void OnSetRotation(object r) {
			if (EntityBody != null)
				EntityBody.Rotation = (float)r;
		}

		bool OnCollision(Fixture a, Fixture b, FarseerPhysics.Dynamics.Contacts.Contact c) {
			var col = new CollisionInfo{ FixtureA = a, FixtureB = b, Contact = c };
			Parent.EventDispatcher.Invoke ("collision", col);
			return true;
		}

		public override void Deserialize (Serialization.Deserializer deserializer)
		{
			return;
		}

		public override void Serialize (Serialization.Serializer serializer)
		{
			return;
		}

		public override EntityComponent Clone ()
		{
			return new BodyComponent ();
		}
	}

	public struct CollisionInfo 
	{
		public Fixture FixtureA, FixtureB;
		public FarseerPhysics.Dynamics.Contacts.Contact Contact;
		public bool Valid;
	}
}

