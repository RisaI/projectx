﻿using System;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace projectx
{
	public static class Input
	{
		public static KeyboardState KBState, prevKBState;
		public static MouseState MState, prevMState;

		public static void Update()
		{
			prevKBState = KBState;
			KBState = Keyboard.GetState ();
			prevMState = MState;
			MState = Mouse.GetState ();
		}

		public static bool KeyPressed(Keys key) {
			return KBState.IsKeyDown (key) && prevKBState.IsKeyUp (key);
		}

		public static bool KeyReleased(Keys key) {
			return KBState.IsKeyUp (key) && prevKBState.IsKeyDown (key);
		}

		public static bool LMBPressed {
			get { return MState.LeftButton == ButtonState.Pressed && prevMState.LeftButton == ButtonState.Released; }
		}

		public static bool RMBPressed {
			get { return MState.RightButton == ButtonState.Pressed && prevMState.RightButton == ButtonState.Released; }
		}

		public static Vector2 MousePosition {
			get { return new Vector2 (MState.X, MState.Y); }
		}
	}
}

