﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using projectx.Game;
using projectx.Game.Entities;

namespace projectx.GameStates
{
	public class GameState_Game : GameState
	{
		public GameWorld World;

		public GameState_Game (Game1 game) : base(game)
		{
			
		}

		public override void Update (GameTime gameTime)
		{
			World.Update (gameTime);
		}

		public override void Draw (SpriteBatch spriteBatch)
		{
			World.Draw (spriteBatch);
		}

		public override void Load ()
		{
			/*Game.Console.World = World = new GameWorld (Game);	
			World.CreateEntity(Vector2.Zero).AddComponent (new SpriteComponent ("ship0")).AddComponent(new BodyComponent(
				new Vector2(-0.1f, -1.8f), new Vector2(0.1f, -1.8f), new Vector2(3, 0.775f), new Vector2(1.6f, 0.4f), new Vector2(0.4f, 1.8f), new Vector2(-0.4f, 1.8f), new Vector2(-1.6f, 0.4f), new Vector2(-3, 0.775f)
			)).AddComponent(new PlayerControllerComponent());
			World.CreateEntity(new Vector2(200, 250)).AddComponent (new SpriteComponent ("ship0")).AddComponent(new BodyComponent(
				new Vector2(-0.1f, -1.8f), new Vector2(0.1f, -1.8f), new Vector2(3, 0.775f), new Vector2(1.6f, 0.4f), new Vector2(0.4f, 1.8f), new Vector2(-0.4f, 1.8f), new Vector2(-1.6f, 0.4f), new Vector2(-3, 0.775f)
			));*/
		}

		public override void Unload ()
		{
			
		}
	}
}

