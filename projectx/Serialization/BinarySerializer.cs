﻿using System;

namespace projectx.Serialization
{
	public class BinarySerializer : Serializer
	{
		public System.IO.BinaryWriter Writer {
			get;
			private set;
		}

		public BinarySerializer (System.IO.Stream stream) : base()
		{
			Writer = new System.IO.BinaryWriter (stream);
		}

		public override void Write (byte b)
		{
			Writer.Write (b);
		}

		public override void Write (byte[] b)
		{
			Writer.Write (b);
		}

		public override void Write (Int16 i) {
			Writer.Write (i);
		}

		public override void Write (Int32 i) {
			Writer.Write (i);
		}

		public override void Write (Int64 i) {
			Writer.Write (i);
		}

		public override void Write (double f)
		{
			Writer.Write (f);
		}

		public override void Write (float f)
		{
			Writer.Write (f);
		}

		public override void Write (string s)
		{
			Writer.Write (s);
		}

		public override void Dispose() {
			Writer.Dispose ();
		}
	}
}

