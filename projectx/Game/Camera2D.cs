﻿using System;
using Microsoft.Xna.Framework;

namespace projectx
{
	public class Camera2D
	{
		private Vector2 _pos;
		public Vector2 Position {
			get { return _pos; }
			set { _pos = value; Recalculate (); }

		}

		public Game1 Game {
			get;
			private set;
		}

		private float _zoom;
		public float Zoom {
			get { return _zoom; }
			set { _zoom = value; Recalculate (); }
		}

		public Camera2D (Game1 game)
		{
			Game = game;
			_zoom = 0.5f;
			Recalculate ();
		}

		private Matrix TranslationMatrix, FTranslationMatrix;

		public void Recalculate() {
			TranslationMatrix = _zoom == 1 ? Matrix.CreateTranslation (Game.Resolution.X / 2 - _pos.X, Game.Resolution.Y / 2 - _pos.Y, 0) : 
				Matrix.CreateScale (Zoom) * Matrix.CreateTranslation (Game.Resolution.X / 2 - _pos.X * _zoom, Game.Resolution.Y / 2 - _pos.Y * _zoom, 0);
			
			FTranslationMatrix = _zoom == 1 ? Matrix.CreateTranslation (-_pos.X / 64f, -_pos.Y / 64f, 0) :
				Matrix.CreateScale(Zoom) * Matrix.CreateTranslation (-_pos.X * _zoom / 64f, -_pos.Y * _zoom / 64f, 0);
		}

		public Matrix GetTranslationMatrix() {
			return TranslationMatrix;
		}

		public Matrix GetFarseerTranslationMatrix() {
			return FTranslationMatrix;
		}

		public Matrix GetFarseerProjectionMatrix() {
			return Matrix.CreateOrthographic(Game.Resolution.X / 64f, -Game.Resolution.Y / 64f, 0, 1);
		}
	}
}

