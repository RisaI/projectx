﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace projectx
{
	public class Game1 : Microsoft.Xna.Framework.Game
	{
		public GraphicsDeviceManager graphics;
		public SpriteBatch spriteBatch;
		public Dictionary<string, GameState> GameStates;
		public GameState CurrentGameState {
			get;
			private set;
		}

		public Vector2 Resolution {
			get { return new Vector2 (graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight); }
			set { graphics.PreferredBackBufferWidth = (int)value.X; graphics.PreferredBackBufferHeight = (int)value.Y;}
		}

		public GameConsole Console {
			get;
			private set;
		}

		public Game1 ()
		{
			IsMouseVisible = true;
			graphics = new GraphicsDeviceManager (this);
			Content.RootDirectory = "Content";
			GameStates = new Dictionary<string, GameState> ();
		}

		protected override void Initialize ()
		{
			base.Initialize ();
		}

		protected override void LoadContent ()
		{
			spriteBatch = new SpriteBatch (GraphicsDevice);
			Loader.Load (this);
			Console = new GameConsole (this);
			GameStates.Add ("menu", new GameStates.GameState_Menu (this));
			GameStates.Add ("game", new GameStates.GameState_Game (this));
			SetGameState ("menu");

			Window.ClientSizeChanged += (sender, e) => {
				Resolution = Window.ClientBounds.Size.ToVector2();
			};
		}

		protected override void Update (GameTime gameTime)
		{
			Input.Update ();
			if (CurrentGameState != null)
				CurrentGameState.Update (gameTime);
			Console.Update (gameTime);
			base.Update (gameTime);
		}

		protected override void Draw (GameTime gameTime)
		{
			graphics.GraphicsDevice.Clear (Color.Black);

			if (CurrentGameState != null)
				CurrentGameState.Draw (spriteBatch);

			Console.Draw (spriteBatch);
			base.Draw (gameTime);
		}

		public void SetGameState(string name) {
			if (CurrentGameState != null)
				CurrentGameState.Unload ();

			if (GameStates.ContainsKey (name))
				(CurrentGameState = GameStates [name]).Load();
		}
	}
}

