﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace projectx.Game.Entities
{
	public class PlayerControllerComponent : EntityComponent
	{
		
		public PlayerControllerComponent ()
		{
			
		}

		public override EntityComponent SetParent (Entity ent)
		{
			if (Parent != null)
				Parent.EventDispatcher.UnregisterEvent("update", this.PlayerInput);
			ent.EventDispatcher.RegisterEvent ("update", this.PlayerInput);
			return base.SetParent (ent);
		}

		public void PlayerInput(object gtime) {
			Parent.World.Camera.Position = Parent.Position;
			var bodyComponent = Parent.GetComponent<BodyComponent> ();
			bool angularDampening = true;
			bodyComponent.EntityBody.AngularDamping = 1f;

			if (Input.KBState.IsKeyDown (Keys.W)) {
				bodyComponent.EntityBody.ApplyForce (Helper.AngleToDirection (Parent.Rotation) * 5);
			}

			if (Input.KBState.IsKeyDown (Keys.S)) {
				bodyComponent.EntityBody.ApplyForce (-Helper.AngleToDirection (Parent.Rotation) * 5);
			}

			if (Input.KBState.IsKeyDown (Keys.A)) {
				bodyComponent.EntityBody.AngularVelocity = MathHelper.Lerp (bodyComponent.EntityBody.AngularVelocity, -1.4f, 0.01f);
				angularDampening = false;
			}

			if (Input.KBState.IsKeyDown (Keys.D)) {
				bodyComponent.EntityBody.AngularVelocity = MathHelper.Lerp (bodyComponent.EntityBody.AngularVelocity, 1.4f, 0.01f);
				angularDampening = false;
			}

			if (angularDampening)
				bodyComponent.EntityBody.AngularDamping = 1.5f;
			else
				bodyComponent.EntityBody.AngularDamping = 0;
		}

		public override void Deserialize (Serialization.Deserializer deserializer)
		{	
			return;
		}

		public override void Serialize (Serialization.Serializer serializer)
		{
			return;
		}

		public override EntityComponent Clone ()
		{
			return new PlayerControllerComponent ();
		}
	}
}

