﻿using System;
using System.IO;
using Newtonsoft.Json;
using Microsoft.Xna.Framework;

namespace projectx.Game.Entities
{
	public abstract class EntityComponent
	{
		public Entity Parent {
			get;
			private set;
		}

		public virtual EntityComponent SetParent(Entity ent) {
			Parent = ent;
			return this;
		}

		public EntityComponent ()
		{
			
		}

		public abstract void Serialize (Serialization.Serializer serializer);
		public abstract void Deserialize (Serialization.Deserializer deserializer);
		public abstract EntityComponent Clone ();
	}
}

