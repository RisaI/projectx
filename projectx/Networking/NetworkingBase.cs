﻿using System;
using Microsoft.Xna.Framework;

namespace projectx
{
	public abstract class NetworkingBase
	{
		public const string APP_NAME = "project_x";

		public NetworkingBase ()
		{
		}
	
		public abstract void Update(GameTime gameTime);
	}
}

