﻿using System;
using Lidgren.Network;

namespace projectx.Serialization
{
	public class NetworkSerializer : Serializer
	{
		private NetOutgoingMessage Msg;
		public NetworkSerializer (NetOutgoingMessage msg)
		{
			Msg = msg;
		}

		public override void Write (byte b)
		{
			Msg.Write (b);
		}

		public override void Write (byte[] b)
		{
			Msg.Write (b);
		}

		public override void Write (double f)
		{
			Msg.Write (f);
		}

		public override void Write (float f)
		{
			Msg.Write (f);
		}

		public override void Write (int i)
		{
			Msg.Write (i);
		}

		public override void Write (long i)
		{
			Msg.Write (i);
		}

		public override void Write (short i)
		{
			Msg.Write (i);
		}

		public override void Write (string s)
		{
			Msg.Write (s);
		}

		public override void Dispose ()
		{
			return;
		}
	}
}

