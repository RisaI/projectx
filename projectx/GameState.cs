﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace projectx
{
	public abstract class GameState
	{
		public Game1 Game {
			get;
			private set;
		}

		public GameState (Game1 game)
		{
			Game = game;	
		}

		public abstract void Update(GameTime gameTime);
		public abstract void Draw(SpriteBatch spriteBatch);


		public abstract void Load ();
		public abstract void Unload ();
	}
}

