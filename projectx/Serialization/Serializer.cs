﻿using System;

namespace projectx.Serialization
{
	public abstract class Serializer : IDisposable
	{
		public Serializer ()
		{
		}

		public abstract void Write(byte b);
		public abstract void Write(string s);
		public abstract void Write(Int16 i);
		public abstract void Write(Int32 i);
		public abstract void Write(Int64 i);
		public abstract void Write(byte[] b);
		public abstract void Write(float f);
		public abstract void Write(double f);

		public void Write(Guid guid) {
			Write (guid.ToByteArray ());
		}

		public abstract void Dispose();
	}
}

