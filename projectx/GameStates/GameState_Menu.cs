﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace projectx.GameStates
{
	public class GameState_Menu : GameState
	{
		public Dictionary<string, Menu> MenuList;
		public string CurrentMenu;

		public GameState_Menu (Game1 game) : base (game)
		{
			MenuList = new Dictionary<string, Menu> () {
				{ "main", new Menu(this, 
					new MenuButton(new Vector2(0, 1), "Test Game", (m) => { m.MenuGameState.Game.SetGameState("game");  }) { Align = AlignMode.Left, offset = new Vector2(10, -75) },
					new MenuButton(new Vector2(0, 1), "Options", (m) => { m.MenuGameState.CurrentMenu = "options"; }) { Align = AlignMode.Left, offset = new Vector2(10, -50) },
					new MenuButton(new Vector2(0, 1), "Quit", (m) => { m.MenuGameState.Game.Exit(); }) { Align = AlignMode.Left, offset = new Vector2(10, -25) }
				) },
				{ "options", new Menu(this, 
					new MenuButton(new Vector2(0, 1), "Back", (m) => { m.MenuGameState.CurrentMenu = "main"; }) { Align = AlignMode.Left, offset = new Vector2(10, -25) }
				) }
			};
		}

		public override void Update(GameTime gameTime) {
			if (MenuList.ContainsKey (CurrentMenu))
				MenuList [CurrentMenu].Update (gameTime);
		}

		public override void Draw(SpriteBatch spriteBatch) {
			if (MenuList.ContainsKey (CurrentMenu))
				MenuList [CurrentMenu].Draw (spriteBatch);
		}

		public override void Load ()
		{
			CurrentMenu = "main";
		}

		public override void Unload ()
		{
			
		}

		public class Menu {
			public GameState_Menu MenuGameState;
			public MenuItem[] MenuItems;

			public Menu(GameState_Menu menu, params MenuItem[] items) {
				MenuGameState = menu;
				MenuItems = items;
			}

			public void Update(GameTime gameTime) {
				if (MenuItems == null)
					return;
				
				for (int i = 0; i < MenuItems.Length; ++i) {
					if (MenuItems [i] != null)
						MenuItems [i].Update (this, gameTime);
				}
			}

			public void Draw(SpriteBatch spriteBatch) {
				if (MenuItems == null)
					return;

				spriteBatch.Begin ();
				for (int i = 0; i < MenuItems.Length; ++i) {
					if (MenuItems [i] != null)
						MenuItems [i].Draw (this, spriteBatch);
				}
				spriteBatch.End ();
			}
		}

		public abstract class MenuItem {
			public Vector2 RelativePosition;

			public MenuItem() {

			}

			public abstract void Update(Menu menu, GameTime gameTime);
			public abstract void Draw(Menu menu, SpriteBatch spriteBatch);
		}

		public class MenuButton : MenuItem {
			static SpriteFont Font {
				get { return Loader.SpriteFonts["hud"]; }
			}

			public Vector2 offset;
			public AlignMode Align;
			public string Text;
			public Action<Menu> ButtonPressAction;

			public MenuButton(Vector2 relativePosition, string text, Action<Menu> pressAction) {
				RelativePosition = relativePosition;
				Text = text;
				ButtonPressAction = pressAction;
			}

			public Rectangle GetButtonArea(Menu menu) {
				Point size = Font.MeasureString (Text).ToPoint ();
				Point position = (RelativePosition * menu.MenuGameState.Game.Resolution + offset).ToPoint ();
				switch (Align) {
				case AlignMode.Center:
					return new Rectangle (position.X - size.X / 2, position.Y - size.Y / 2, size.X, size.Y);
				case AlignMode.Left:
					return new Rectangle (position.X, position.Y - size.Y / 2, size.X, size.Y);
				case AlignMode.Right:
					return new Rectangle (position.X - size.X, position.Y - size.Y / 2, size.X, size.Y);
				default:
					return new Rectangle ();
				}
			}

			public override void Update (Menu menu, GameTime gameTime)
			{
				if (GetButtonArea(menu).Contains (Input.MousePosition)) {
					if (Input.LMBPressed && ButtonPressAction != null)
						ButtonPressAction.Invoke (menu);
				}
			}

			public override void Draw (Menu menu, SpriteBatch spriteBatch)
			{
				spriteBatch.DrawString (Font, Text ?? "", GetButtonArea(menu).Location.ToVector2(), Color.White, 0, Vector2.Zero, 1f, SpriteEffects.None, 0f);
			}
		}

		public enum AlignMode {
			Center,
			Left,
			Right
		}
	}
}