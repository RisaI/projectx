﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace projectx.Game.Entities
{
	public class SpriteComponent : EntityComponent
	{
		public string AssetName {
			get;
			private set;
		}

		public byte FramesX, FramesY;
		public Int16 CurrentFrame;

		public Point Size {
			get { return new Point (Texture.Width / FramesX, Texture.Height / FramesY); }
		}

		public Vector2 Origin {
			get {
				var s = Size;
				return new Vector2 (s.X / 2, s.Y / 2);
			}
		}

		public Rectangle SourceRectangle {
			get {
				var s = Size;
				return new Rectangle(new Point((CurrentFrame % FramesX) * s.X, (CurrentFrame / FramesX) * s.Y), s);
			}
		}

		public SpriteComponent (string assetName)
		{
			AssetName = assetName;
			FramesX = FramesY = 1;
		}

		public Texture2D Texture {
			get { return Loader.Textures [AssetName]; }
		}

		public override EntityComponent SetParent (Entity ent)
		{
			if (Parent != null)
				Parent.EventDispatcher.UnregisterEvent ("draw", this.Draw);
			ent.EventDispatcher.RegisterEvent ("draw", this.Draw);
			return base.SetParent (ent);
		}

		public override void Deserialize (Serialization.Deserializer deserializer)
		{
			AssetName = deserializer.ReadString ();
			FramesX = deserializer.ReadByte ();
			FramesY = deserializer.ReadByte ();
			CurrentFrame = deserializer.ReadInt16 ();
		}

		public override void Serialize (Serialization.Serializer serializer)
		{
			serializer.Write (AssetName);
			serializer.Write (FramesX);
			serializer.Write (FramesY);
			serializer.Write (CurrentFrame);
		}

		public void Draw(object sprt) {
			var spriteBatch = sprt as SpriteBatch;
			var texture = Texture;

			spriteBatch.Draw (texture, Parent.Position, SourceRectangle, Color.White, Parent.Rotation, Origin, Vector2.One, SpriteEffects.None, 0f);
		}

		public override EntityComponent Clone ()
		{
			return new SpriteComponent (AssetName) { FramesX = FramesX, FramesY = FramesY, CurrentFrame = CurrentFrame };
		}
	}
}

