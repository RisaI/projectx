﻿using System;
using System.IO;
using Lidgren.Network;

namespace projectx.Serialization
{
	public abstract class NetworkDeserializer : Deserializer
	{
		private NetIncomingMessage Msg;

		public NetworkDeserializer (NetIncomingMessage msg)
		{
			Msg = msg;
		}

		public override byte ReadByte() {
			return Msg.ReadByte ();
		}

		public override string ReadString() {
			return Msg.ReadString ();
		}

		public override Int16 ReadInt16() {
			return Msg.ReadInt16 ();
		}

		public override Int32 ReadInt32() {
			return Msg.ReadInt32 ();
		}

		public override Int64 ReadInt64() {
			return Msg.ReadInt64 ();
		}

		public override byte[] ReadBytes(int count) {
			return Msg.ReadBytes (count);
		}

		public override float ReadSingle() {
			return Msg.ReadSingle ();
		}
			
		public override float ReadDouble() {
			return Msg.ReadDouble ();
		}

		public override void Dispose ()
		{
			return;
		}
	}
}

