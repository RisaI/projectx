﻿using System;
using projectx.Game;
using Lidgren.Network;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace projectx
{
	public class NetworkingClient : NetworkingBase
	{
		public Game1 Game {
			get;
			private set;
		}

		public GameWorld World {
			get;
			private set;
		}

		public NetClient Client {
			get;
			private set;
		}

		public NetworkingClient (Game1 game)
		{
			Game = game;
			var config = new NetPeerConfiguration (NetworkingBase.APP_NAME);
			Client = new NetClient (config);
		}

		public void Connect(string ip, int port) {
			Client.Connect (ip, port);
		}

		public void Disconnect(string byeMsg) {
			Client.Disconnect (byeMsg);
			World = null;
		}

		public override void Update (GameTime gameTime)
		{
			NetIncomingMessage msg;
			while ((msg = Client.ReadMessage ()) != null) {
				ProcessMessage (msg);
				Client.Recycle (msg);
			}

			if (World != null)
				World.Update (gameTime);
		}

		void ProcessMessage(NetIncomingMessage msg) {
			switch (msg.MessageType) {
			case NetIncomingMessageType.Data:
				ProcessData (msg);		
				break;
			}
		}

		void ProcessData(NetIncomingMessage msg) {
			switch (msg.ReadByte ()) {
			case 0x00:
				switch (msg.ReadByte ()) {
				case 0x00:
					World = new GameWorld (this);
					World.Deserialize (new projectx.Serialization.NetworkDeserializer (msg));
					break;
				}
				break;
			}
		}
	}
}

