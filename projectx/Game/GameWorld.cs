﻿using System;
using System.Collections.Generic;
using projectx.Game.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace projectx.Game
{
	public class GameWorld
	{
		public const float TICK = 2000;

		private bool isServer;
		public bool IsServer {
			get { return isServer; }
		}

		public bool IsClient {
			get { return !isServer; }
		}

		public NetworkingBase Networking {
			get;
			private set;
		}

		public Camera2D Camera {
			get;
			private set;
		}

		public EventDispatcher EventDispatcher {
			get;
			private set;
		}

		public float TimeScale = 1f;

		public FarseerPhysics.Dynamics.World Physics;
		public List<Entity> Entities;

		public FarseerPhysics.DebugView.DebugViewXNA PhysicsDebug;

		GameWorld(NetworkingBase networking) {
			Networking = networking;
			Entities = new List<Entity> ();
			Physics = new FarseerPhysics.Dynamics.World (Vector2.Zero);
			EventDispatcher = new EventDispatcher ();
		}

		public GameWorld (NetworkingClient client) : this((NetworkingBase)client)
		{
			isServer = false;
			Camera = new Camera2D (client.Game);
			PhysicsDebug = new FarseerPhysics.DebugView.DebugViewXNA (Physics) { Enabled = true };
			PhysicsDebug.LoadContent (client.Game.GraphicsDevice, client.Game.Content);
		}

		public GameWorld (NetworkingServer server) : this((NetworkingBase)server) {
			isServer = true;
		}

		public Entity AddEntity(Entity ent) {
			Entities.Add (ent.SetGameWorld(this));
			EventDispatcher.Invoke ("entity_add", ent);
			return ent;
		}

		public Entity CreateEntity(Vector2 position, float rotation = 0) {
			return AddEntity(new Entity () { Position = position, Rotation = rotation });
		}

		float tickTimer;
		public void Update(GameTime gameTime) {
			bool tick = false;
			if (tickTimer >= TICK) {
				tickTimer -= TICK;
				tick = true;
			}
			tickTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
			EventDispatcher.Invoke ("update_prephysics", gameTime);
			Physics.Step ((float)gameTime.ElapsedGameTime.TotalSeconds * TimeScale);
			EventDispatcher.Invoke ("update_preentity", gameTime);
			for (int i = 0; i < Entities.Count; ++i) {
				Entities [i].Update (gameTime, tick);
			}
			Entities.RemoveAll (ent => ent.RemovalPending);
			EventDispatcher.Invoke ("update_postentity", gameTime);
		}

		public void Draw(SpriteBatch spriteBatch) {
			if (isServer)
				return;

			spriteBatch.Begin (SpriteSortMode.FrontToBack, null, null, null, null, null, Camera.GetTranslationMatrix());
			for (int i = 0; i < Entities.Count; ++i) {
				Entities [i].Draw (spriteBatch);
			}
			spriteBatch.End ();
			if (PhysicsDebug.Enabled) {
				PhysicsDebug.RenderDebugData (Camera.GetFarseerProjectionMatrix(), Camera.GetFarseerTranslationMatrix ());
			}
		}

		public void Serialize(Serialization.Serializer serializer) {
			/*serializer.Write (Camera.Position.X);
			serializer.Write (Camera.Position.Y);*/
			serializer.Write (TimeScale);
			serializer.Write ((Int16)Entities.Count);
			for (int i = 0; i < Entities.Count; ++i) {
				Entities [i].Serialize (serializer);
			}
		}

		public void Deserialize(Serialization.Deserializer deserializer) {
			//Camera.Position = new Vector2 (deserializer.ReadSingle(), deserializer.ReadSingle());
			TimeScale = deserializer.ReadSingle ();
			int entCount = deserializer.ReadInt16 ();
			for (int i = 0; i < entCount; ++i) {
				var ent = new Entity ();
				ent.Deserialize (deserializer);
				AddEntity (ent);
			}
		}
	}
}