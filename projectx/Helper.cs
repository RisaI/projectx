﻿using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace projectx
{
	public static class Helper
	{
		public static Vector2 AngleToDirection(float angle) {
			return new Vector2((float)Math.Sin(angle), (float)-Math.Cos(angle));
		}

		public static float DirectionToAngle(Vector2 direction) {
			return (float)Math.Atan2 (-direction.Y, direction.X);
		}

		public static void DrawRectangle(this SpriteBatch spriteBatch, Rectangle rect, Color clr, float depth) {
			DrawRectangle (spriteBatch, rect.Location.ToVector2 (), rect.Size.ToVector2 (), clr, depth);
		}

		public static void DrawRectangle(this SpriteBatch spriteBatch, Vector2 position, Vector2 size, Color clr, float depth) {
			spriteBatch.Draw (Loader.Pixel, position, null, clr, 0f, Vector2.Zero, size, SpriteEffects.None, depth);
		}

		public static void DrawLine(this SpriteBatch spriteBatch, Vector2 pos1, Vector2 pos2, Color clr, float depth)
		{
			var dst = pos2 - pos1;
			spriteBatch.Draw(Loader.Pixel, pos1, null, clr, -DirectionToAngle(dst), new Vector2(0, 0.5f), new Vector2(dst.Length(), 1), SpriteEffects.None, depth);
		}

		public static void Write(this BinaryWriter writer, Vector2 vec)
		{
			writer.Write (vec.X);
			writer.Write (vec.Y);
		}

		public static Vector2 ReadVector2(this BinaryReader reader)
		{
			return new Vector2 (reader.ReadSingle(), reader.ReadSingle());
		}
	}

	public static class SerializationHelper
	{
		static Dictionary<string, Type> KnownTypes = new Dictionary<string, Type>();

		public static Type GetType(string typeName) {
			if (!KnownTypes.ContainsKey (typeName))
				KnownTypes.Add (typeName, Type.GetType (typeName));
			return KnownTypes [typeName];
		}
	}
}

