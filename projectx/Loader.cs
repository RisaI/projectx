﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using projectx.Game.Entities;

namespace projectx
{
	public static class Loader
	{
		public static Dictionary<string, Texture2D> Textures = new Dictionary<string, Texture2D>();
		public static Dictionary<string, SpriteFont> SpriteFonts = new Dictionary<string, SpriteFont>();
		public static Dictionary<string, Game.Entities.Entity> Entities = new Dictionary<string, Game.Entities.Entity>();
		/*public static Dictionary<string, SoundEffect> Music = new Dictionary<string, SoundEffect>();
		public static Dictionary<string, Effect> Shaders = new Dictionary<string, Effect>();*/

		public static Texture2D Pixel {
			get;
			private set;
		}

		public static void Load(Game1 game)
		{
			Pixel = new Texture2D (game.GraphicsDevice, 1, 1);
			Pixel.SetData<Color> (new Color[] { Color.White });

			string rootDir = "textures/", frootDir = "fonts/"/*, mrootDir = "Music/", srootDir = "Shaders/", prootDir = "Particles/"*/;

			//Textures
			Textures.Add("ship0", game.Content.Load<Texture2D>(rootDir + "ship0"));
			Textures.Add("ship1", game.Content.Load<Texture2D>(rootDir + "ship1"));
			Textures.Add("ship2", game.Content.Load<Texture2D>(rootDir + "ship2"));
			Textures.Add("ship3", game.Content.Load<Texture2D>(rootDir + "ship3"));
			Textures.Add("ship4", game.Content.Load<Texture2D>(rootDir + "ship4"));

			//Fonts
			SpriteFonts.Add("hud", game.Content.Load<SpriteFont>(frootDir + "hud"));
			SpriteFonts.Add("console", game.Content.Load<SpriteFont>(frootDir + "console"));

			//Entities
			Entities.Add("player", new projectx.Game.Entities.Entity()
				.AddComponent (new SpriteComponent ("ship0"))
				.AddComponent(new BodyComponent(
				new Vector2(-0.1f, -1.8f), new Vector2(0.1f, -1.8f), new Vector2(3, 0.775f), new Vector2(1.6f, 0.4f),
				new Vector2(0.4f, 1.8f), new Vector2(-0.4f, 1.8f), new Vector2(-1.6f, 0.4f), new Vector2(-3, 0.775f)))
				.AddComponent(new PlayerControllerComponent()));
		}

		public static string GamePath
		{
			get { return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location); }
		}

		public static string CombineGamePath(string path)
		{
			return System.IO.Path.Combine(GamePath, path);
		}
	}
}

