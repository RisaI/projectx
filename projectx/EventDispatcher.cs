﻿using System;
using System.Collections.Generic;

namespace projectx
{
	public class EventDispatcher
	{
		Dictionary<string, List<Action<object>>> Listeners;

		public EventDispatcher ()
		{
			Listeners = new Dictionary<string, List<Action<object>>> ();
		}

		public void RegisterEvent(string evnt, Action<object> action) {
			if (Listeners.ContainsKey (evnt))
				Listeners [evnt].Add (action);
			else
				Listeners.Add (evnt, new List<Action<object>> () { action });
		}

		public void UnregisterEvent(string evnt, Action<object> action) {
			if (Listeners.ContainsKey (evnt)) {
				Listeners [evnt].Remove (action);
				if (Listeners [evnt].Count == 0)
					Listeners.Remove (evnt);
			}
		}

		public void Invoke(string evnt, object args) {
			if (Listeners.ContainsKey (evnt)) {
				var l = Listeners [evnt];
				for (int i = 0; i < l.Count; ++i) {
					l [i].Invoke (args);
				}
			}
		}
	}
}