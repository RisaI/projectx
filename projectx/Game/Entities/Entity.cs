﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace projectx.Game.Entities
{
	public sealed class Entity
	{
		public GameWorld World {
			get;
			private set;
		}

		public bool RemovalPending {
			get;
			private set;
		}
		
		public Vector2 Position;
		public float Rotation;
		public Guid UID {
			get;
			private set;
		}

		public EventDispatcher EventDispatcher;
		public List<EntityComponent> Components;

		public Entity ()
		{
			EventDispatcher = new EventDispatcher ();
			Components = new List<EntityComponent> ();
			UID = Guid.NewGuid ();
		}

		public Entity AddComponent(EntityComponent component) {
			Components.Add (component);
			if (World != null)
				component.SetParent (this);
			return this;
		}

		public T GetComponent<T>() where T : EntityComponent {
			for (int i = 0; i < Components.Count; ++i)
				if (Components [i] is T)
					return (T)Components [i];
			return null;
		}

		public void Update(GameTime gameTime, bool tick) {
			EventDispatcher.Invoke ("preupdate", gameTime);
			EventDispatcher.Invoke ("update", gameTime);
			EventDispatcher.Invoke ("postupdate", gameTime);
			if (tick)
				EventDispatcher.Invoke ("tick", gameTime);
		}

		public void Draw(SpriteBatch spriteBatch) {
			EventDispatcher.Invoke ("draw", spriteBatch);
		}

		public void Remove() {
			EventDispatcher.Invoke ("remove", null);
			if (World != null)
				World.EventDispatcher.Invoke ("entity_remove", this);
			RemovalPending = true;
		}

		public Entity SetPosition(Vector2 position) {
			Position = position;
			EventDispatcher.Invoke ("setpos", position);
			return this;
		}

		public Entity SetRotation(float rotation) {
			Rotation = rotation;
			EventDispatcher.Invoke ("setrot", rotation);
			return this;
		}

		public Entity SetGameWorld(GameWorld world) {
			World = world;
			for (int i = 0; i < Components.Count; ++i)
				Components [i].SetParent (this);
			return this;
		}

		public void Serialize(Serialization.Serializer serializer) {
			serializer.Write (Position.X);
			serializer.Write (Position.Y);
			serializer.Write (Rotation);
			serializer.Write (UID);
			serializer.Write ((byte)Components.Count);
			for (int i = 0; i < Components.Count; ++i) {
				var comp = Components [i];
				serializer.Write (comp.GetType().AssemblyQualifiedName);
				comp.Serialize (serializer);
			}
		}

		public void Deserialize(Serialization.Deserializer deserializer) {
			Position = new Vector2 (deserializer.ReadSingle (), deserializer.ReadSingle ());
			Rotation = deserializer.ReadSingle ();
			UID = deserializer.ReadGuid ();
			byte compc = deserializer.ReadByte ();
			for (int i = 0; i < compc; ++i) {
				EntityComponent comp = System.Runtime.Serialization.FormatterServices.GetSafeUninitializedObject (SerializationHelper.GetType (deserializer.ReadString ())) as EntityComponent;
				comp.Deserialize (deserializer);
				Components.Add (comp);
			}
		}

		public Entity Clone() {
			Entity clone = new Entity () { Position = Position, Rotation = Rotation, UID = Guid.NewGuid() }.SetGameWorld(World);
			for (int i = 0; i < Components.Count; ++i) {
				clone.AddComponent (Components [i].Clone ());
			}
			return clone;
		}
	}
}

