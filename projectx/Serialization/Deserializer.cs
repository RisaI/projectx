﻿using System;

namespace projectx.Serialization
{
	public abstract class Deserializer : IDisposable
	{
		public Deserializer ()
		{
		}

		public abstract byte ReadByte();
		public abstract string ReadString();
		public abstract Int16 ReadInt16();
		public abstract Int32 ReadInt32();
		public abstract Int64 ReadInt64();
		public abstract byte[] ReadBytes(int count);
		public abstract float ReadSingle();
		public abstract float ReadDouble();

		public override void Dispose ();

		public Guid ReadGuid() {
			return new Guid (ReadBytes (16));
		}
	}
}

