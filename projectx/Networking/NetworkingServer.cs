﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Lidgren.Network;
using projectx.Game;

namespace projectx
{
	public class NetworkingServer : NetworkingBase
	{
		public NetServer Server {
			get;
			private set;
		}

		public Game.GameWorld World {
			get;
			private set;
		}

		public List<PlayerConnection> Connections {
			get;
			private set;
		}

		public NetworkingServer ()
		{
			var config = new NetPeerConfiguration (NetworkingBase.APP_NAME);
			config.Port = 8009;
			config.UseMessageRecycling = true;
			Server = new NetServer (config);
			Connections = new List<PlayerConnection> ();
		}

		public PlayerConnection GetPlayer(NetConnection con) {
			PlayerConnection c = Connections.Find(ca => ca.Connection == con);
			if (c == null) {
				c = new PlayerConnection(con);
				Connections.Add(c);
			}
			return c;
		}

		public void Start(GameWorld world) {
			if (world == null)
				return;
				
			World = world;
			//Hooks
			World.EventDispatcher.RegisterEvent ("entity_add", OnEntityAdd);
			World.EventDispatcher.RegisterEvent ("entity_remove", OnEntityRemoved);

			Server.Start ();
		}

		void OnEntityAdd(object obj) {
			var ent = obj as Game.Entities.Entity;
			var msg = Server.CreateMessage ();
			msg.Write (0x01);
			msg.Write (0x00);
			var serializer = new Serialization.NetworkSerializer (msg);
			ent.Serialize (serializer);
			Server.SendToAll (msg, NetDeliveryMethod.ReliableOrdered);
		}

		void OnEntityRemoved(object obj) {
			var ent = obj as Game.Entities.Entity;
			var msg = Server.CreateMessage ();
			msg.Write (0x01);
			msg.Write (0x01);
			msg.Write (ent.UID.ToByteArray ());
		}

		void SendWorld(NetConnection con) {
			var msg = Server.CreateMessage ();
			msg.Write (0x00);
			msg.Write (0x00);
			var serializer = new Serialization.NetworkSerializer (msg);
			World.Serialize (serializer);
			Server.SendMessage (msg, con, NetDeliveryMethod.ReliableOrdered);
		}

		public void Stop(string msg) {
			Server.Shutdown (msg);
			Connections.Clear ();
			World = null;
		}

		public override void Update (Microsoft.Xna.Framework.GameTime gameTime)
		{
			if (Server.Status == NetPeerStatus.Running) {
				NetIncomingMessage msg;
				while ((msg = Server.ReadMessage ()) != null) {
					ProcessMessage (msg);
					Server.Recycle (msg);
				}
			}

			if (World != null)
				World.Update (gameTime);
		}

		void ProcessMessage(NetIncomingMessage msg) {
			switch (msg.MessageType) {
			case NetIncomingMessageType.StatusChanged:
				var player = GetPlayer (msg.SenderConnection);
				if (player.Connection.Status == NetConnectionStatus.Disconnected) {
					if (player.PlayerEntity != null)
						player.PlayerEntity.Remove ();
					Connections.Remove (player);
				} else if (player.Connection.Status == NetConnectionStatus.Connected) {
					player.PlayerEntity = World.AddEntity (Loader.Entities ["player"].Clone ());
				}
				break;
			case NetIncomingMessageType.ConnectionApproval:
				msg.SenderConnection.Approve ();

				break;
			}
		}

		public class PlayerConnection
		{
			public NetConnection Connection {
				get;
				private set;
			}

			public Guid UID {
				get;
				private set;
			}

			public Game.Entities.Entity PlayerEntity {
				get;
				set;
			}

			public PlayerConnection(NetConnection con) {
				Connection = con;
				UID = Guid.NewGuid();
			}
		}
	}
}

