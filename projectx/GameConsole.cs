﻿using System;
using System.Text.RegularExpressions;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.Linq;

namespace projectx
{
	public class GameConsole
	{
		public const int HEIGHT = 300;

		public Game1 Game {
			get;
			private set;
		}

		public Game.GameWorld World {
			get;
			set;
		}

		public SpriteFont Font {
			get { return Loader.SpriteFonts["console"]; }
		}

		private int _off;
		public int Offset{
			get { return _off; }
			set {
				_off = MathHelper.Clamp(value, 0, (int)Font.MeasureString(Output).Y - HEIGHT);
			}
		}
		public bool Enabled;
		public string PreviousInput
		{
			get;
			private set;
		}
		public string Input
		{
			get;
			private set;
		}

		public Dictionary<string, ConsoleCommand> Commands;

		public GameConsole (Game1 game)
		{
			Game = game;
			Game.Window.TextInput += (sender, e) => {
				if (Enabled && e.Character != '`' && e.Character != '~' && e.Key != Keys.Back && e.Key != Keys.Enter) 
				{
					Input += e.Character;
				}
			};

			var quitCommand = new ConsoleCommand ("Quits the game", (a) => {
				Game.Exit ();
			});
			Commands = new Dictionary<string, ConsoleCommand> () {
				{ "listentities", new ConsoleCommand("Lists all entities spawned in the current world", (args) => { 
					if (World == null)
						return;

					for (int i = 0; i < World.Entities.Count; ++i) {
						WriteLine(i + ": GUID - " + World.Entities[i].UID.ToString());
					}
				}) },
				{ "help", new ConsoleCommand("Prints out all of the commands and their descriptions", (a) => { Commands.ToList().ForEach(p => { WriteLine(p.Key + " - " + p.Value.Description); }); }) },
				{ "echo", new ConsoleCommand("Prints out your text", (a) => { for (int i = 0; i < a.Length; ++i) { WriteLine(a[i]); } }) },
				{ "quit", quitCommand },
				{ "exit", quitCommand }
			};
		}

		public void Update(GameTime gameTime) {
			if (projectx.Input.KeyPressed (Keys.OemTilde))
				Enabled = !Enabled;

			if (!Enabled)
				return;
			if (projectx.Input.KeyPressed(Keys.Up) && !string.IsNullOrWhiteSpace(PreviousInput))
			{
				Input = PreviousInput;
			}
			if (Input != null)
			{
				if (projectx.Input.KeyPressed(Keys.Enter))
				{
					Input = Input.Trim();

					if (!string.IsNullOrWhiteSpace(Input))
					{
						string command = Input.Split(' ')[0],
						remainder = Input.Substring(command.Length).Trim();
						Regex reg = new Regex(@"(\""[^\""]*\"")|(\b\S+\b)");
						var matches = reg.Matches(remainder);
						string[] args = new string[matches.Count];
						for (int i = 0; i < matches.Count; ++i)
						{
							args[i] = matches[i].Value.Replace("\"", string.Empty);
						}
						Command(command, args);
						PreviousInput = Input;
					}

					Input = null;
				}
				else if (projectx.Input.KeyPressed(Keys.Back))
				{
					if (Input.Length > 0)
						Input = Input.Substring(0, Input.Length - 1);

				}
				if (projectx.Input.KeyPressed(Keys.Escape))
					Input = null;
			}

			if (projectx.Input.KeyPressed(Keys.PageUp))
			{
				Offset += SCROLL;
			}
			else if (projectx.Input.KeyPressed(Keys.PageDown))
			{
				Offset -= SCROLL;
			}
		}

		public void Draw(SpriteBatch spriteBatch) {
			if (!Enabled)
				return;
			int LineHeight = (int)Font.MeasureString("I").Y;

			spriteBatch.GraphicsDevice.ScissorRectangle = new Rectangle(0, 0, (int) Game.Resolution.X, HEIGHT);
			spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, null, null, new RasterizerState() { ScissorTestEnable = true });
			spriteBatch.DrawRectangle(Vector2.Zero, new Vector2(Game.Resolution.X, HEIGHT), new Color(0, 50, 120, 200), 0f);

			if (!string.IsNullOrEmpty(Output))
			{
				var outputHeight = Font.MeasureString(Output).Y;
				if (outputHeight > HEIGHT)
				{
					spriteBatch.DrawString(Font, Output, new Vector2(0, HEIGHT), Color.White, 0f, new Vector2(0, outputHeight - Offset), 1f, SpriteEffects.None, 1f);
				}
				else
				{
					spriteBatch.DrawString(Font, Output, Vector2.Zero, Color.White);
				}
			}
			spriteBatch.End();

			spriteBatch.GraphicsDevice.ScissorRectangle = new Rectangle(0, HEIGHT, (int) Game.Resolution.X, LineHeight);
			spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, null, null, new RasterizerState() { ScissorTestEnable = true });
			spriteBatch.DrawRectangle(new Vector2(0, HEIGHT), new Vector2(Game.Resolution.X, LineHeight), new Color(0, 30, 120, 225), 0f);
			if (!string.IsNullOrEmpty(Input))
				spriteBatch.DrawString(Font, Input, new Vector2(0, HEIGHT), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);

			spriteBatch.End();
		}

		public void Command(string command, string[] args) {
			command = command.ToLower ();
			if (Commands.ContainsKey (command))
				Commands [command].Invoke (args);
			else
				WriteLine("Command '" + command + "' not found.");
		}

		string Output;
		public void WriteLine(string line) {
			Output += (!string.IsNullOrEmpty(Output) ? "\r\n" : null) + line;
		}

		const int SCROLL = 125;
	}

	public class ConsoleCommand {
		public string Description;
		public Action<string[]> Action;


		public ConsoleCommand(Action<string[]> action) {
			Action = action;
		}

		public ConsoleCommand(string description, Action<string[]> action) : this (action) {
			Description = description;
		}

		public void Invoke(string[] args) {
			if (Action != null)
				Action.Invoke (args);
		}
	}
}

