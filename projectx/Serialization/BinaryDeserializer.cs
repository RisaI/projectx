﻿using System;
using System.IO;

namespace projectx.Serialization
{
	public abstract class BinaryDeserializer : Deserializer
	{
		private BinaryReader Reader;

		public BinaryDeserializer (Stream input)
		{
			Reader = new BinaryReader (input);
		}

		public override byte ReadByte() {
			return Reader.ReadByte ();
		}

		public override string ReadString() {
			return Reader.ReadString ();
		}

		public override Int16 ReadInt16() {
			return Reader.ReadInt16 ();
		}

		public override Int32 ReadInt32() {
			return Reader.ReadInt32 ();
		}

		public override Int64 ReadInt64() {
			return Reader.ReadInt64 ();
		}

		public override byte[] ReadBytes(int count) {
			return Reader.ReadBytes (count);
		}

		public override float ReadSingle() {
			return Reader.ReadSingle ();
		}
			
		public override float ReadDouble() {
			return Reader.ReadDouble ();
		}

		public override void Dispose ()
		{
			Reader.Dispose ();
		}
	}
}

